<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03: Classes and Objects</title>
</head>
<body>
    <h1>Objects from Variables</h1>
    <p><?php var_dump($buildingObj); ?></p>
    <!-- Object operator or single arrow notation is used to access/call methods and properties in PHP ojbect. -->
    <p><?php echo $buildingObj->name; ?></p>

    <h1>Objects from Classes</h1>
    <p><?php var_dump($building); ?></p>
    <!-- Accessing the method of the instantiated object -->
    <p><?php echo $building->printName(); ?></p>

    <h2>Modifying the Instantiated Object</h2>
    <?php $building->name = "GMA Network"; ?>
    <!-- Change the floor number, but provide a value of string instead of int. -->
    <!-- This issue can be solved with encapsulation later on. -->
    <?php //$building->floors = "twenty"; ?>
    <p><?php var_dump($building); ?></p>
    <p><?php echo $building->printName(); ?></p>

    <h1>Inheritance (Condominium Object)</h1>
    <p><?php var_dump($condominium); ?></p>
    <p><?= $condominium->name;  ?></p>
    <p><?= $condominium->floors;  ?></p>
    <p><?= $condominium->address;  ?></p>
    <p><?= $condominium->printName();  ?></p>
</body>
</html>